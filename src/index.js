import React from 'react';
import ReactDOM from 'react-dom';
import App from './app.jsx';
import './styles.css';
import Footer from './footer.jsx'
import Header from './header.jsx'

ReactDOM.render(
  <Header />,
  document.getElementById('header')
);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <Footer />,
  document.getElementById('footer')
);


