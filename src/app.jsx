import React from 'react'
import Item from './todo-item.jsx'
import DATA from './todos'

class App extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            todos: DATA,
            newTodo: ""
        }
        this.handleClick = this.handleClick.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleDelete = this.handleDelete.bind(this)
    }

    handleClick(text){
        this.setState(prevState => {
            const updateState = prevState.todos.map(todo => {
                if(todo.text === text) {
                   return {
                        ...todo,
                        complete: !todo.complete
                    }
                }
                return todo
            })
            return {
                todos: updateState
            }
        })
        
    }

    handleDelete(text) {
        this.setState(prevState => {
            const updateTodos = prevState.todos.filter((todo) => todo.text !== text)
            return {
                todos: updateTodos
            }
        })
    }

    handleChange(event){
        const {name, value} = event.target
        // by updating state.value we ensure the component is the only truth holder (controlled components)
        this.setState({
            [name]: value 
        })
    }

    handleSubmit(event){
        let todo = {
            text: this.state.newTodo,
            complete: false
        }
        
        this.setState((prevState) => {
            const updateTodos = [todo, ...prevState.todos]
            return {
                todos: updateTodos
            }
                    
        })    
        event.preventDefault();    
    }

    



    render(){
        const toDos = this.state.todos.map(todo => <Item key={todo.text} item={todo} handleClick={this.handleClick} handleDelete={this.handleDelete}/>)
        return (
            <div className="wrapper">
                <div className="row">
                    <form onSubmit={this.handleSubmit}>
                        <input type="text" onChange={this.handleChange} name="newTodo" value={this.state.newTodo} placeholder="your new todo"/>
                        <button type="submit">Add todo</button>
                    </form>
                </div>
                <ul className="todo-list row">
                    {toDos}
                </ul>
                
            </div>
            
            )
    
    }
}


export default App