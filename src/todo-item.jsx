import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'


function Item(props) {
    
    const completeStyle = {
        color: "#00b894",
        backgroundColor: "#55efc4",
        textDecoration: "line-through"
    }
    return(
        <li className="item" 
            style={props.item.complete ? completeStyle : null} 
            onClick={() => props.handleClick(props.item.text)}>{props.item.text}
            <span>
                <FontAwesomeIcon icon={faTrash} onClick={() => props.handleDelete(props.item.text)}/>
            </span>
        </li>
   
    )
}

export default Item